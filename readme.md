## Usage
Clone, edit config:
```
git clone git@gitlab.com:carverhaines/mariadb-dump-docker.git
cd mariadb-dump-docker
cp .env.example .env
vim .env
```

Copy to your existing repo:
```
cat .env >> /path/to/repo/.env
cat docker_compose.yml >> /path/to/repo/docker-compose.yml
```
